=== Alert Notice Boxes ===
Contributors: yehi
Tags: alert, alert notice box, alert, alert box, notice, notification, notification alert, message, popup
Requires at least: 4.2
Tested up to: 4.7.2
Stable tag: 4.7.2
License: GPLv2 or later

Alert Notice Boxes allows you to create beautiful custom alerts that appear on pages or posts of your choice.

== Description ==

Alert Notice Boxes allows you to create beautiful custom alerts that appear on pages or posts of your choice.

Demo - <a href="http://wp-plugins-demos.madadim.co.il/" target="_blank">Alert Notice Boxes - Demo</a>

= Features: =

* Easy to Use
* Lightweight
* Create Unlimited Notification Alerts Boxes
* Responsive
* Add Custom text
* Possible to add photos
* Show on Post Typs / Posts / Pages
* Selecting Designs
* Control Delay
* Control Show Time

= Coming soon: =

* Control The Screen Position
* More Designs
* Choosing which devices appears

= Feedback =
If you like this plugin, then please leave us a good rating and review.<br> Consider following us on <a rel="author" href="https://plus.google.com/109974551206892069425">Google+</a>, and <a href="https://www.facebook.com/madadim.co.il">Facebook</a>

== Installation ==

Upload the Alert Notice Boxes plugin to your plugin folder on your site, Activate it.

== Screenshots ==

1. Display notifications on the site
2. The list of alerts
3. Edit the contents of an alert
4. Alert Notice settings, will soon be more options
5. Options for displaying alerts on a specific page

== Changelog ==

= 1.2.5.7 =
*Release Date - 21/02/17*

* Fix: close button

= 1.2.5.6 =
*Release Date - 02/02/17*

* Fix: save alerts

= 1.2.5.5 =
*Release Date - 26/01/17*

* Fix: small fixes in PHP
* Improve: admin menu


= 1.2.5.4 =
*Release Date - 24/01/17*

* Fix: small fixes in PHP


= 1.2.5.3 =
*Release Date - 23/01/17*

* Fix: small fixes in PHP

= 1.2.5.2 =
*Release Date - 19 October 2016*

* Fix: Cancellation notice when alert in trash

= 1.2.5.1 =
*Release Date - 08 October 2016*

* add: Capabilities to administrator role

= 1.2.5 =
*Release Date - 16 August 2016*

* Fix: Suitable PHP version:  native (5.4) - thanks to dap19335 member
* Fix: Option for On / Off the message now works - thanks to dap19335 member

= 1.2.4 =
*Release Date - 12 August 2016*

* Add: Cancellation alerts in selected page
* Add: Display alerts in selected page

= 1.2.3 =
*Release Date - 03 August 2016*

* Fix: Now you can enter a long title without an error saving

= 1.2.2 =
*Release Date - 29 July 2016*

* Add: Control Delay
* Add: Control Show Time

= 1.2.1 =
*Release Date - 27 July 2016*

* Add: Selecting designs
* Add: Now you can also display HTML


= 1.1 =
*Release Date - 25 July 2016*

* Improve: Security


= 1.0 =
*Release Date - 7 July 2016*

* Hello World